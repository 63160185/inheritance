/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.inheritance;

/**
 *
 * @author ACER
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Blac&White");
        dang.speak();
        dang.walk();

        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();

        Dog mome = new Dog("Mome", "Blac&White");
        mome.speak();
        mome.walk();

        Dog bat = new Dog("Bat", "Blac&White");
        bat.speak();
        bat.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.walk();
        som.fly();

        Duck gabgab = new Duck("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();

        System.out.println("---------------------");

        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = som;
        ani2 = zero;
        som.fly();

        System.out.println("Ani1 : zom is Duck : " + (ani1 instanceof Duck));

        System.out.println("---------------------");

        Animal[] animals = {dang, zero, som, to, mome, bat, gabgab};
        for (int i = 0; i < animals.length; i++) {
            animals[i].walk();
            animals[i].speak();

            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
        }

        System.out.println("---------------------");

        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("Animal is Animal : " + (animal instanceof Animal));
        System.out.println("dang is Animal : " + (dang instanceof Animal));
        System.out.println("dang is Dog : " + (dang instanceof Dog));
        System.out.println("mome is Animal : " + (mome instanceof Animal));
        System.out.println("mome is Dog : " + (mome instanceof Dog));
        System.out.println("to is Animal : " + (to instanceof Animal));
        System.out.println("to is Dog : " + (to instanceof Dog));
        System.out.println("bat is Animal : " + (bat instanceof Animal));
        System.out.println("bat is Dog : " + (bat instanceof Dog));
        System.out.println("zero is Animal : " + (zero instanceof Animal));
        System.out.println("zero is Cat : " + (zero instanceof Cat));
        System.out.println("som is Animal : " + (som instanceof Animal));
        System.out.println("som is Duck : " + (som instanceof Duck));
        System.out.println("som is Object : " + (som instanceof Object));
        System.out.println("gabgab is Animal : " + (gabgab instanceof Animal));
        System.out.println("gabgab is Duck : " + (gabgab instanceof Duck));

    }
}
